unit DbGridEhLayoutProcessor;
(******************************************************************************
  LayoutKeeper: ���������� ���������� ��� DbGridEh.
  ������������ (���������, ���������������) ������� � ������ �������� �����.
  ��� ������������� ������� ������������ FieldName.

  (�) ������ ����������, 2021. ��������: LGPL.
  https://bitbucket.org/danik-ik/layoutkeeper
 ******************************************************************************)

interface
uses
  LayoutKeeper, DbGridEh;

(******************************************************************************
  ����������� ��������� � ILayoutKeeper (����� Add).
  ������������ (���������, ���������������) ������� � ������ �������� �����.
  @Param Ctrl: �������������� �����
  @Returns: ���������, �������������� ��������� �����
 ******************************************************************************)
function newDbGridEhLayoutProcessor(Grid: TDbGridEh): ILayoutProcessor;

implementation
uses
  StrUtils, SysUtils, OeStrUtil;

type
  TDgBridEhLayoutProcessor = class(TAbstractLayoutProcessor, ILayoutProcessor)
  protected
    Grid: TDBGridEh;
    function getLayout(): string; override;
    procedure setLayout(value: string); override;
  public
    constructor Create(Grid: TDBGridEh);
  end;

function newDbGridEhLayoutProcessor(Grid: TDbGridEh): ILayoutProcessor;
var it: TDgBridEhLayoutProcessor;
begin
  it := TDgBridEhLayoutProcessor.Create(Grid);
  result := it;
end;

{ TDgBridEhLayoutProcessor }

constructor TDgBridEhLayoutProcessor.Create(Grid: TDBGridEh);
begin
  inherited Create(Grid);
  self.Grid := Grid;
end;

function TDgBridEhLayoutProcessor.getLayout: string;
var i: integer;
begin
  result := '';
  for i := 0 to Grid.Columns.Count - 1 do
    result := result + Grid.Columns[i].FieldName + ':' + IntToStr(Grid.Columns[i].Width) + ';';
end;

procedure TDgBridEhLayoutProcessor.setLayout(value: string);
var
  i, j: integer;
  columnDefs, KeyVal : TStringArray;
  Column: TColumnEh;
  FieldName: string;
  ColWidth: integer;
begin
  columnDefs := split(value);
  for i := 0 to high(columnDefs) do
  begin
    KeyVal := split(columnDefs[i], ':');
    FieldName := KeyVal[0];
    ColWidth := StrToInt(KeyVal[1]);
    for j := 0 to Grid.Columns.Count - 1 do
    begin
      Column := Grid.Columns[j];
      if AnsiCompareText(FieldName, Column.FieldName) = 0 then
      begin
        Column.Width := ColWidth;
        Column.Index := i;
        break;
      end;
    end;
  end;
end;

end.
