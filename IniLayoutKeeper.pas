unit IniLayoutKeeper;
(******************************************************************************
  LayoutKeeper: ���������� ��������� � �������������� ini-�����.
  
  (�) ������ ����������, 2021. ��������: LGPL.
  https://bitbucket.org/danik-ik/layoutkeeper
 ******************************************************************************)

interface
uses
  LayoutKeeper, IniFiles;

(******************************************************************************
  ������ ���������� ���������.
  @Param ini: ini-����
  @Param iniSection: ������ ini-�����, �� ���������: [Layouts]
  @Returns: ��������� ����������� ���������
 ******************************************************************************)
function newIniLayoutKeeper(ini: TIniFile; iniSection: string = 'Layouts'): ILayoutKeeper;

implementation

type
  TIniLayoutKeeper = class(TAbstractLayoutKeeper)
  private
     ini: TIniFile;
     iniSection: string;
  protected
    function readValue(key: string): string; override;
    procedure WriteValue(key, Value: string); override;
  end;

function newIniLayoutKeeper(ini: TIniFile; iniSection: string): ILayoutKeeper;
var it: TIniLayoutKeeper;
begin
  it := TIniLayoutKeeper.Create;
  result := it;

  it.ini := ini;
  it.iniSection := iniSection
end;

{ TIniLayoutKeeper }

function TIniLayoutKeeper.readValue(key: string): string;
begin
  result := ini.ReadString(iniSection, key, '');
end;

procedure TIniLayoutKeeper.WriteValue(key, Value: string);
begin
  ini.WriteString(iniSection, key, Value);
end;

end.

