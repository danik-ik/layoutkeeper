unit LayoutKeeper;
(******************************************************************************
  ���������� ��� ���������� ��������� ���������� ��������� VCL ����
  (�) ������ ����������, 2021. ��������: LGPL.
  https://bitbucket.org/danik-ik/layoutkeeper

  ����������:

  ��� ���������� ������������ ���������� �������� ���� ����������
  TAbstractLayoutProcessor. �������������� ����������� ������.
  ��� ������������� �������������� calculateKey.
  �������: TWidthLayoutProcessor, THeightLayoutProcessor

  ��� ���������� � ������������ ��������� �������� ���� ����������
  TAbstractLayoutKeeper. �������������� ����������� ������.
 ******************************************************************************)

interface
uses
  Controls;

type
(******************************************************************************
  ���������� ILayoutProcessor ����������� ������������ �������������� ��������
  � ��� ��������� ������������� � �������.
  ����� ���������� ��������� ����, ��� ������� ����� ��������� ������ ��������.
  �� ��������� ���� ���� ��� �����\[�����\[...]]���������, ��� ������
  �������� - Name (���� ����������) ��� ClassName. ���� �����������
  ������������� �������� ����� ������ ��������� ������ �������� - �������������
  �������� ������� (��. TWidthLayoutProcessor, THeightLayoutProcessor)
 ******************************************************************************)
  ILayoutProcessor = interface
    (** ��������� ���������� ���� ��� ��������� *)
    function getKey: string;
    (** ����������� ������� ��������� � ������ ��� ���������� *)
    function getLayout: string;
    (** �������� ����� ����������� ��������� � ���� ������ � ��������������� � *)
    procedure setLayout(value: string);
  end;

(******************************************************************************
  ���������� ILayoutKeeper ������������ ���������� � ������������� ���������
  � �������������� ������������� key-value ���������.

  ������: ��. IniLayoutKeeper
 ******************************************************************************)
  ILayoutKeeper = interface
    procedure add(processor: ILayoutProcessor);
    procedure save;
    procedure restore;
  end;

  TAbstractLayoutKeeper = class(TInterfacedObject, ILayoutKeeper)
  private
    processors: array of ILayoutProcessor;
  protected
    function readValue(key: string): string; virtual; abstract;
    procedure WriteValue(key, Value: string); virtual; abstract;
  public
    procedure add(processor: ILayoutProcessor);
    procedure save;
    procedure restore;
  end;

  TAbstractLayoutProcessor = class(TInterfacedObject, ILayoutProcessor)
  private
    key: String;
  protected
    ctl: TWinControl; // ������������ ��� ������������ ����� �� ���������
    function CalculateKey: string; virtual;
  public
    function getKey: string;
    function getLayout: string; virtual; abstract;
    procedure setLayout(value: string); virtual; abstract;
    constructor Create(ctl: TWinControl);
  end;

(******************************************************************************
  ����������� ��������� � ILayoutKeeper (����� Add).
  ������������ (���������, ���������������) ������ ����������
  @Param Ctrl: �������������� ���������
  @Returns: ���������, �������������� ������ ����������
 ******************************************************************************)
function newWidthLayoutProcessor(ctrl: TWinControl): ILayoutProcessor;

(******************************************************************************
  ����������� ��������� � ILayoutKeeper (����� Add).
  ������������ (���������, ���������������) ������ ����������
  @Param Ctrl: �������������� ���������
  @Returns: ���������, �������������� ������ ����������
 ******************************************************************************)
function newHeightLayoutProcessor(ctrl: TWinControl): ILayoutProcessor;

(******************************************************************************
  ����������� ��������� � ILayoutKeeper (����� Add).
  ������������ (���������, ���������������) ������������� ������ ����������.
  � �������� ���� ������������ ���������������� ��������.
  @Param Ctrl: �������������� ���������
  @Returns: ���������, �������������� ������������� ������ ����������
 ******************************************************************************)
function newRelativeWidthLayoutProcessor(ctrl: TWinControl; Min: extended = 0.1; Max: extended = 0.9): ILayoutProcessor;

(******************************************************************************
  ����������� ��������� � ILayoutKeeper (����� Add).
  ������������ (���������, ���������������) ������������� ������ ����������
  � �������� ���� ������������ ���������������� ��������.
  @Param Ctrl: �������������� ���������
  @Returns: ���������, �������������� ������������� ������ ����������
 ******************************************************************************)
function newRelativeHeightLayoutProcessor(ctrl: TWinControl; Min: extended = 0.1; Max: extended = 0.9): ILayoutProcessor;

implementation
uses
  Forms, SysUtils;

type
  TWidthLayoutProcessor = class (TAbstractLayoutProcessor)
  protected
    function CalculateKey: string; override;
  public
    function getLayout: string; override;
    procedure setLayout(value: string); override;
  end;

  THeightLayoutProcessor = class (TAbstractLayoutProcessor)
  protected
    function CalculateKey: string; override;
  public
    function getLayout: string; override;
    procedure setLayout(value: string); override;
  end;

  TAbstractRelativeLayoutProcessor = class (TAbstractLayoutProcessor)
  protected
    max, min: extended;
    parentCtl: TWinControl;
  public
    function getLayout: string; override;
    procedure setLayout(value: string); override;
    procedure setParam(value: integer); virtual; abstract;
    function getParam: integer; virtual; abstract;
    function getParentParam: integer; virtual; abstract;
    constructor Create(ctl: TWinControl; Min: extended = 0.1; Max: extended = 0.9);
  end;

  TRelativeWidthLayoutProcessor = class (TAbstractRelativeLayoutProcessor)
  protected
    procedure setParam(value: integer); override;
    function getParam: integer; override;
    function getParentParam: integer; override;
    function CalculateKey: string; override;
  end;

  TRelativeHeightLayoutProcessor = class (TAbstractRelativeLayoutProcessor)
  protected
    procedure setParam(value: integer); override;
    function getParam: integer; override;
    function getParentParam: integer; override;
    function CalculateKey: string; override;
  end;

function newWidthLayoutProcessor(ctrl: TWinControl): ILayoutProcessor;
begin
  result := TWidthLayoutProcessor.Create(ctrl);
end;

function newHeightLayoutProcessor(ctrl: TWinControl): ILayoutProcessor;
begin
  result := THeightLayoutProcessor.Create(ctrl);
end;

function newRelativeWidthLayoutProcessor(ctrl: TWinControl; Min: extended = 0.1; Max: extended = 0.9): ILayoutProcessor;
begin
  result := TRelativeWidthLayoutProcessor.Create(ctrl, Min, Max);
end;

function newRelativeHeightLayoutProcessor(ctrl: TWinControl; Min: extended = 0.1; Max: extended = 0.9): ILayoutProcessor;
begin
  result := TRelativeHeightLayoutProcessor.Create(ctrl, Min, Max);
end;

{ TAbstractLayoutProcessor }

function TAbstractLayoutProcessor.CalculateKey: string;
var
  key: string;
  currentCtl: TWinControl;

  function getCtlName(ctl:TWinControl): string;
  begin
    if ctl.Name = '' then
      result := ctl.ClassName
    else
      result := ctl.Name;
  end;
begin
  key := getCtlName(self.ctl);
  currentCtl := self.ctl.Parent;
  while assigned(currentCtl) do
  begin
    if (currentCtl is TFrame)
    or (currentCtl is TForm) then
      key := getCtlName(currentCtl) + '\' + key;

    currentCtl := currentCtl.Parent;
  end;
  Result := key;
end;

constructor TAbstractLayoutProcessor.Create(ctl: TWinControl);
begin
  self.ctl := ctl;
  key := calculateKey;
end;

function TAbstractLayoutProcessor.getKey: string;
begin
  result := self.key;
end;

{ TWidthLayoutProcessor }

function TWidthLayoutProcessor.CalculateKey: string;
begin
  result := inherited CalculateKey + '_width';
end;

function TWidthLayoutProcessor.getLayout: string;
begin
  result := IntToStr(self.ctl.width);
end;

procedure TWidthLayoutProcessor.setLayout(value: string);
begin
  if value <> '' then
    self.ctl.width := StrToInt(value);
end;

{ THeightLayoutProcessor }

function THeightLayoutProcessor.CalculateKey: string;
begin
  result := inherited CalculateKey + '_height';
end;

function THeightLayoutProcessor.getLayout: string;
begin
  result := IntToStr(self.ctl.height);
end;

procedure THeightLayoutProcessor.setLayout(value: string);
begin
  if value <> '' then
    self.ctl.height := StrToInt(value);
end;

{ TAbstractLayoutKeeper }

procedure TAbstractLayoutKeeper.add(processor: ILayoutProcessor);
begin
  setLength(processors, length(processors) + 1);
  self.processors[high(processors)] := processor;
end;

procedure TAbstractLayoutKeeper.restore;
var i: integer;
begin
  for i := 0 to high(processors) do
    self.processors[i].setLayout(readValue(self.processors[i].getKey));
end;

procedure TAbstractLayoutKeeper.save;
var i: integer;
begin
  for i := 0 to high(processors) do
    WriteValue(self.processors[i].getKey, self.processors[i].getLayout);
end;

{ TAbstractRelativeLayoutProcessor }

constructor TAbstractRelativeLayoutProcessor.Create(ctl: TWinControl; Min,
  Max: extended);
begin
  inherited Create(ctl);
  self.parentCtl := self.ctl.Parent;
  self.min := min;
  self.max := max;
end;

function TAbstractRelativeLayoutProcessor.getLayout: string;
var
  value: extended;
begin
  if getParentParam = 0 then exit;
  value := getParam / getParentParam;
  if value < min then value := min;
  if value > max then value := max;
  Result := FloatToStr(value);
end;

procedure TAbstractRelativeLayoutProcessor.setLayout(value: string);
var
  relative: extended;
begin
  inherited;
  if Value = '' then exit;
  try
    relative := StrToFloat(Value);
  except
    exit;
  end;
  if relative < min then relative := min;
  if relative > max then relative := max;
  setParam(round(getParentParam * relative));
end;

{ TRelativeWidthLayoutProcessor }

function TRelativeWidthLayoutProcessor.CalculateKey: string;
begin
  Result := inherited CalculateKey + '_relWidth';
end;

function TRelativeWidthLayoutProcessor.getParam: integer;
begin
  Result := ctl.Width;
end;

function TRelativeWidthLayoutProcessor.getParentParam: integer;
begin
  Result := parentCtl.ClientWidth;
end;

procedure TRelativeWidthLayoutProcessor.setParam(value: integer);
begin
  ctl.Width := value;
end;

{ TRelativeHeightLayoutProcessor }

function TRelativeHeightLayoutProcessor.CalculateKey: string;
begin
  Result := inherited CalculateKey + '_relHeight';
end;

function TRelativeHeightLayoutProcessor.getParam: integer;
begin
  Result := ctl.Height;
end;

function TRelativeHeightLayoutProcessor.getParentParam: integer;
begin
  Result := parentCtl.ClientHeight;
end;

procedure TRelativeHeightLayoutProcessor.setParam(value: integer);
begin
  ctl.Height := value;
end;

end.
