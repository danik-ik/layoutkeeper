unit DbGridLayoutProcessor;
(******************************************************************************
  LayoutKeeper: ���������� ���������� ��� DbGrid.
  ������������ (���������, ���������������) ������� � ������ �������� �����.
  ��� ������������� ������� ������������ FieldName.

  (�) ������ ����������, 2021. ��������: LGPL.
  https://bitbucket.org/danik-ik/layoutkeeper
 ******************************************************************************)

interface
uses
  LayoutKeeper, DbGrids;

(******************************************************************************
  ����������� ��������� � ILayoutKeeper (����� Add).
  ������������ (���������, ���������������) ������� � ������ �������� �����.
  @Param Ctrl: �������������� �����
  @Returns: ���������, �������������� ��������� �����
 ******************************************************************************)
function newDbGridLayoutProcessor(Grid: TDbGrid): ILayoutProcessor;

implementation
uses
  StrUtils, SysUtils, OeStrUtil;

type
  TDgBridEhLayoutProcessor = class(TAbstractLayoutProcessor, ILayoutProcessor)
  protected
    Grid: TDBGrid;
    function getLayout(): string; override;
    procedure setLayout(value: string); override;
  public
    constructor Create(Grid: TDBGrid);
  end;

function newDbGridLayoutProcessor(Grid: TDbGrid): ILayoutProcessor;
var it: TDgBridEhLayoutProcessor;
begin
  it := TDgBridEhLayoutProcessor.Create(Grid);
  result := it;
end;

{ TDgBridEhLayoutProcessor }

constructor TDgBridEhLayoutProcessor.Create(Grid: TDBGrid);
begin
  inherited Create(Grid);
  self.Grid := Grid;
end;

function TDgBridEhLayoutProcessor.getLayout: string;
var i: integer;
begin
  result := '';
  for i := 0 to Grid.Columns.Count - 1 do
    result := result + Grid.Columns[i].FieldName + ':' + IntToStr(Grid.Columns[i].Width) + ';';
end;

procedure TDgBridEhLayoutProcessor.setLayout(value: string);
var
  i, j: integer;
  columnDefs, KeyVal : TStringArray;
  Column: TColumn;
  FieldName: string;
  ColWidth: integer;
begin
  columnDefs := split(value);
  for i := 0 to high(columnDefs) do
  begin
    KeyVal := split(columnDefs[i], ':');
    FieldName := KeyVal[0];
    ColWidth := StrToInt(KeyVal[1]);
    for j := 0 to Grid.Columns.Count - 1 do
    begin
      Column := Grid.Columns[j];
      if AnsiCompareText(FieldName, Column.FieldName) = 0 then
      begin
        Column.Width := ColWidth;
        Column.Index := i;
        break;
      end;
    end;
  end;
end;

end.
