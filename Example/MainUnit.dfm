object MainForm: TMainForm
  Left = 372
  Top = 116
  Width = 1305
  Height = 675
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 89
    Top = 115
    Width = 10
    Height = 522
    Color = clRed
    MinSize = 40
    ParentColor = False
  end
  object Splitter2: TSplitter
    Left = 0
    Top = 105
    Width = 1289
    Height = 10
    Cursor = crVSplit
    Align = alTop
    Color = clRed
    ParentColor = False
  end
  object Grid: TDBGrid
    Left = 99
    Top = 115
    Width = 1190
    Height = 522
    Align = alClient
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'a'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'b'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'd'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'e'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'f'
        Visible = True
      end>
  end
  object SidePanel: TPanel
    Left = 0
    Top = 115
    Width = 89
    Height = 522
    Align = alLeft
    Constraints.MinWidth = 40
    TabOrder = 1
    DesignSize = (
      89
      522)
    object SaveButton: TButton
      Left = 8
      Top = 40
      Width = 67
      Height = 25
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Save'
      TabOrder = 0
      OnClick = SaveButtonClick
    end
    object LoadButton: TButton
      Left = 8
      Top = 8
      Width = 67
      Height = 25
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Load'
      TabOrder = 1
      OnClick = LoadButtonClick
    end
  end
  object InfoPanel: TPanel
    Left = 0
    Top = 0
    Width = 1289
    Height = 105
    Align = alTop
    TabOrder = 2
    object About: TMemo
      Left = 1
      Top = 1
      Width = 1287
      Height = 103
      Align = alClient
      Lines.Strings = (
        #1047#1072#1087#1091#1089#1090#1080' '#1084#1077#1085#1103'!')
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
end
