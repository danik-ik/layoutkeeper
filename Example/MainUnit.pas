unit MainUnit;
(******************************************************************************
  ���������� ��� ���������� ��������� ���������� ��������� VCL ����
  ������ �������������.

  (�) ������ ����������, 2021. ��������: LGPL.

  ��, ��� ���������:
  - ������� ��������� ���������, ������� � �������� � ���� ����������
    ��� ����������� ����������:

    private
      Layout: ILayoutKeeper;

    procedure TForm1.FormCreate(Sender: TObject);
      // ������� ������� ���-���� (������������� �� ������� ���������� IniLayoutKeeper)
      ini := TIniFile.Create('.\Layout.ini');

      // ������� ��������� ���������.
      // ��������� ����� �� ����!!! ���������� ������������� �� �������� ������
      // ��� �������� ���������� Layout (������� �����, ���� ��� ���� ������ �����)
      Layout := newIniLayoutKeeper(ini);

      // ����� ���������������� ��������� �����
      Layout.add( newDbGridLayoutProcessor(Grid) );

      // ����� ���������������� ������ ������� ������
      Layout.add( newWidthLayoutProcessor(SidePanel) );

      // ����� ���������������� ������ ������� ������
      Layout.add( newHeightLayoutProcessor(InfoPanel) );

  - � ������ ������ ������������ �������� ��� ����������
      Layout.restore;
      Layout.save;

  - ��� ������������� ������� ����������� ���������� ���������� ��� ���������

 ******************************************************************************)

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, StdCtrls, LayoutKeeper, IniLayoutKeeper,
  DbGridLayoutProcessor, IniFiles, ExtCtrls;

type
  TMainForm = class(TForm)
    Grid: TDBGrid;
    SidePanel: TPanel;
    SaveButton: TButton;
    LoadButton: TButton;
    Splitter1: TSplitter;
    InfoPanel: TPanel;
    Splitter2: TSplitter;
    About: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure LoadButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    Layout: ILayoutKeeper;
    ini: tinifile;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation
uses
  StrUtils;

{$R *.dfm}

procedure TMainForm.FormCreate(Sender: TObject);
begin
  ini := TIniFile.Create('.\Layout.ini');
  Layout := newIniLayoutKeeper(ini, 'lol');   // ��������� ����� �� ����!!! ���������� ������������� �� �������� ������
                                              // ��� �������� ���������� Layout (� ������ ������ ������� �����)
  Layout.add(NewDbGridLayoutProcessor(Grid));         // ����� ���������������� ��������� �����
  Layout.add(newWidthLayoutProcessor(SidePanel));     // ����� ���������������� ������ ������� ������
  Layout.add(newHeightLayoutProcessor(InfoPanel));    // ����� ���������������� ������ ������� ������

  About.Lines.LoadFromFile('..\README.md');
  About.Lines.text := Utf8ToAnsi(About.Lines.text);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  ini.Free;
end;

procedure TMainForm.FormActivate(Sender: TObject);
begin
  Layout.restore; // ������������ ����� ���������� ���������
end;

procedure TMainForm.FormDeactivate(Sender: TObject);
begin
  Layout.save;    // ��������� ���������
end;

procedure TMainForm.LoadButtonClick(Sender: TObject);
begin
  Layout.restore;
end;

procedure TMainForm.SaveButtonClick(Sender: TObject);
begin
  Layout.save;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Layout.save;
end;

end.
