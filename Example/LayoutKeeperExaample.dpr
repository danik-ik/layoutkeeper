program LayoutKeeperExaample;

(******************************************************************************
  LayoutKeeper
  ���������� ��� ���������� ��������� ���������� ��������� VCL ����.
  ������ �������������.

  (�) ������ ����������, 2021. ��������: LGPL.
  https://bitbucket.org/danik-ik/layoutkeeper
 ******************************************************************************)

uses
  Forms,
  MainUnit in 'MainUnit.pas' {MainForm},
  OeStrUtil in '..\OeStrUtil.pas',
  DbGridLayoutProcessor in '..\DbGridLayoutProcessor.pas',
  IniLayoutKeeper in '..\IniLayoutKeeper.pas',
  LayoutKeeper in '..\LayoutKeeper.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
