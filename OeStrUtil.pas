unit OeStrUtil;
(******************************************************************************
  ������ ����������. �������� �������� (����� ��������) ������ ��������,
  ������� ������ ���� ��������� � �������� ���, ������� �� ������������ 
  ������������ � ����������� �������.
  ����� ������ � �����, ������ ��� ������ ���������� (����������� ��� ������� 
  ����� ����������� ��������� ������). 
  
  (�) ������ ����������, 2021. ��������: LGPL.
  https://bitbucket.org/danik-ik/layoutkeeper
 ******************************************************************************)

interface

type
  TStringArray = array of string;

(******************************************************************************
  ����������� ������ � ������ �� ����������� (������������������ ��������).
  ������ �������� � ����� �������������.
  @Param source: �������� ������
  @Param delimiter: ����������� (������������������ ��������, �������� #13#10
    ��� ���������� �� �������)
  @Returns: ������ ����� (���������� ��������� ���������)
 ******************************************************************************)
function split(const source: string; const delimiter: string = ';'): TStringArray;

implementation
uses
  StrUtils;

function split(const source: string; const delimiter: string = ';'): TStringArray;
var
  cursor: integer;
  delimiterPos: integer;
  delimiterLength: integer;
begin
  SetLength(Result, 0);
  cursor := 1;
  delimiterLength := length(delimiter);
  delimiterPos := PosEx(delimiter, source);
  while delimiterPos > 0 do
  begin
    setlength(result, length(result) + 1);
    result[length(result) - 1] := copy(source, cursor, delimiterPos - cursor);
    cursor := delimiterPos + delimiterLength;
    delimiterPos := PosEx(delimiter, source, cursor);
  end;
  if cursor <= length(source) then
  begin
    setlength(result, length(result) + 1);
    result[length(result) - 1] := copy(source, cursor, length(source));
  end;
end;

end.
